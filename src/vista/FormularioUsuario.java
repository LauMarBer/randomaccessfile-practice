package vista;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

import Res.Constantes;
import modelo.Empleado;

public class FormularioUsuario {
Scanner teclado = new Scanner(System.in);
	
	//Muestra las opciones del men�
	public void muestraMenu() {
		for (String enunciado : Constantes.ENUNCIADO) {
			System.out.println(enunciado);
		}
	}

	//Permite introducir por la consola un entero
	public int getNumberInt() {
		int numero=0;
		boolean correcto=false;
		
		do {
			try {
				numero=teclado.nextInt();
				teclado.nextLine();
				correcto=true;
			}catch(InputMismatchException e) {
				teclado.nextLine();
				System.err.println(Constantes.FORMAT_ERROR_MESSAGE);
			}
		}while(!correcto);
		
		
		return numero;
		
	}
	
	//Permite introducir por la consola un double
	public double getNumberDouble() {
		double numero=0;
		boolean correcto=false;
		
		do {
			try {
				numero=teclado.nextDouble();
				teclado.nextLine();
				correcto= true;
			}catch(InputMismatchException e) {
				teclado.nextLine();
				System.err.println(Constantes.FORMAT_ERROR_MESSAGE);
			}
		}while(!correcto);
		
		return numero;
		
	}
	
	//Permite introducir por la consola un long
	public long getNumberLong() {
		long numero=0;
		boolean correcto = false;
		
		do {
			try {
				numero=teclado.nextLong();
				teclado.nextLine();
				correcto = true;
			}catch(InputMismatchException e) {
				teclado.nextLine();
				System.err.println(Constantes.FORMAT_ERROR_MESSAGE);
			}
		}while(!correcto);
		
		
		return numero;
		
	}

	//Permite introducir por consola un texto
	public String getText() {
		String linea;
		
		do {
			linea=teclado.nextLine();
			if(linea.length()>20) {
				System.err.println(Constantes.NAME_WARNING_MESSAGE);
			}
			
		}while(linea.length()>20);
	
		return linea;
	}
	
	
	public boolean ConfirmarOperacion() {
		int respuesta;
		boolean confirm = false;
		
		do {
			System.out.println(Constantes.DELETE_CONFIRM_REQUEST);
			respuesta = getNumberInt();
			
			switch(respuesta) {
				case 1:
					confirm = true;
					break;
				case 0:
					break;
				default:
					System.err.println(Constantes.OPTION_ERROR_MESSAGE);
			}
		}while(respuesta!=1 && respuesta!=0);
		
		return confirm;
	}
	
	public void elegirCampo() {
		
		for (String enunciado : Constantes.MODIFICA_EMPLE_CAMPOS) {
			System.out.println(enunciado);
		}
		
		
	}
	
	
	public Empleado introduceEmpleado() {
		Empleado e = new Empleado();
		
		for (int i = 0; i < 2; i++) {
			System.out.println(Constantes.INSERTA_DATOS_EMPLE.get(i));
		}
		System.out.print(Constantes.INSERTA_DATOS_EMPLE.get(2));
		e.setNombre_emple(getText());
		
		System.out.print(Constantes.INSERTA_DATOS_EMPLE.get(3));
		e.setDepto_emple(getNumberInt());
		
		System.out.print(Constantes.INSERTA_DATOS_EMPLE.get(4));
		e.setSalario_emple(getNumberDouble());
		
		return e;
	}
	
	
	public void muestraEmpleado(Empleado e) {
		System.out.println("-----------------DATOS DEL EMPLEADO-----------------");
		System.out.println("C�digo de empleado: " + e.getId_emple());
		System.out.println("Nombre: " + e.getNombre_emple());
		System.out.println("N�mero de departamento: " + e.getDepto_emple());
		System.out.println("Salario: " + e.getSalario_emple() + "�");
		System.out.println();
	}
	
	public void muestraEmpleMayorSueldo(ArrayList<Empleado> empleados, double sueldo) {
		for (Empleado empleado : empleados) {
			if(empleado.getSalario_emple()==sueldo) {
				muestraEmpleado(empleado);
			}
		}
	}
	
	public void muestraDptoMasEmple(HashMap<Integer, Integer> dpto_emple, int num_dpto) {
		System.out.println("El departamento con m�s n�mero de empleados es: " + num_dpto + " con un total de " + dpto_emple.get(num_dpto) + " empleados.");
		System.out.println();
	}
	
	public void muestraDptoMasGasto(HashMap<Integer, Double> gasto_dpto, int clave_hashmap) {
		
		System.out.println("El departamento con mayor gasto en salario es: " + clave_hashmap + " con un gasto total de " + gasto_dpto.get(clave_hashmap) + " �.");
		System.out.println();
	}
	
	public void tablaCase01(ArrayList<Empleado> empleados) {

		System.out.println("--------------------------------------TABLA EMPLEADOS-------------------------------------");
		System.out.printf("%-15s", Constantes.TITULOS_CASE_01[0]);
		System.out.printf("%-25s", Constantes.TITULOS_CASE_01[1]);
		System.out.printf("%-15s", Constantes.TITULOS_CASE_01[2]);
		System.out.printf("%13s", Constantes.TITULOS_CASE_01[3]);
		System.out.println();
		
		for (Empleado empleado : empleados) {
			System.out.printf("%-15d", empleado.getId_emple());
			System.out.printf("%-25s", empleado.getNombre_emple());
			System.out.printf("%-15d", empleado.getDepto_emple());
			System.out.printf("%13.2f", empleado.getSalario_emple());
			System.out.println();
		}
		
		System.out.println("-------------------------------------------------------------------------------------------");
		System.out.println();
	}
}
