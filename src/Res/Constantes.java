package Res;

import java.util.ArrayList;
import java.util.Arrays;

public class Constantes {
	
	public static final String RUTA_FILE ="EMPLE.dat";
	public static final String RUTA_FILE_COPIA ="EMPLE_copia.dat";
	
	public static final ArrayList<String> ENUNCIADO = new ArrayList<>(
			Arrays.asList("***********ADMINISTRACI�N DE EMPLEADOS*************",
							"Por favor, introduce una de las siguiente opciones:",
							"1. Mostrar todos los empleados",
							"2. Buscar un empleado por su c�digo identificativo",
							"3. Insertar un nuevo empleado",
							"4. Borrar un empleado mediante su c�digo indentificativo",
							"5. Modificar un empleado mediante su c�digo identificativo",
							"6. Mostrar los empleados que m�s ganan",
							"7. Realizar un borrado f�sico sobre los registros eliminados de forma l�gica",
							"8. Mostrar el departamento que m�s empleados tiene",
							"9. Mostrar el departamento que m�s gasta en salarios", 
							"0. Salir"));
	
	public static final ArrayList<String> INSERTA_DATOS_EMPLE = new ArrayList<>(
			Arrays.asList("***************NUEVO EMPLEADO*****************",
							"Por favor, rellena los siguientes campos:",
							"Nombre (20 caracteres como m�ximo): ",
							"N�mero de departamento (n�mero entero): ",
							"Salario (n�mero decimal): "));
	
	public static final ArrayList<String> MODIFICA_EMPLE_CAMPOS = new ArrayList<>(
			Arrays.asList("***************MODIFICAR EMPLEADO*****************",
							"�Qu� campo deseas modificar? Elige uno de los siguientes: ",
							"1. Nombre",
							"2. N�mero de departamento: ",
							"3. Salario: ",
							"0. Cancelar operaci�n"));
	
	public static final String[] TITULOS_CASE_01 = new String[] {
			"IDENTIFICADOR",
			"NOMBRE",
			"DEPARTAMENTO",
			"SALARIO"
	};
	
	public static final String EMPLE_COD_REQUEST = "Por favor, introduce el c�digo del empleado: ";
	public static final String EMPLE_NAME_REQUEST = "Por favor, introduce el nuevo nombre: ";
	public static final String EMPLE_DPTO_REQUEST = "Por favor, introduce el nuevo c�digo de departamento: ";
	public static final String EMPLE_SALARIO_REQUEST = "Por favor, introduce el nuevo salario: ";
	public static final String DELETE_CONFIRM_REQUEST = "�Est� seguro? Pulse 1 para confirmar y 0 para cancelar";
	
	
	public static final String CREATEFILE_ERROR_MESSAGE = "No se ha podido crear el archivo correctamente, por lo que no se puede acceder a �l";
	public static final String EXCEPTION_ERROR_MESSAGE = "Ha ocurrido un problema durante la manipulaci�n del archivo. Por favor, reinicie la aplicaci�n e int�ntelo de nuevo";
	public static final String RENAMING_DELETING_ERROR = "Los nuevos ficheros no se han podido gestionar correctamente";
	public static final String OPTION_ERROR_MESSAGE = "El n�mero introducido no corresponde a ninguna opci�n";
	public static final String FORMAT_ERROR_MESSAGE = "El formato introducido no es correcto. Por favor, introduzca el dato en el formato solicitado";
	public static final String NAME_WARNING_MESSAGE = "Este campo tiene un tama�o m�ximo de 20 caracteres. Por favor, intente no exceder este tama�o";	
	public static final String NOTFOUND_ERROR_MESSAGE = "El fichero al que desea acceder no existe en la ruta especificada";
	
	public static final String EMPTY_FILE_MESSAGE = "El fichero est� vac�o. Introduzca nuevos registros con la opci�n del men� principal: \"Insertar un nuevo empleado\"";
	public static final String DELETED_EMPLE_MESSAGE = "El empleado al que desea acceder ha sido eliminado previamente";
	public static final String NOTEXISTING_EMPLE_MESSAGE = "El empleado al que desea acceder no existe";	
	public static final String BYE_MESSAGE = "�Hasta pronto!";
	public static final String SUCCESS_PROCESS = "�Operaci�n realizada correctamente!";
	public static final String CANCELED_MESSAGE = "Operaci�n cancelada";
}
