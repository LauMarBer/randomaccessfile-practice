package modelo;

public class Empleado {
	private int id_emple;
	private String nombre_emple;
	private int depto_emple;
	private double salario_emple;
	
	public Empleado() {
		this.id_emple = 0;
		this.nombre_emple = null;
		this.depto_emple = 0;
		this.salario_emple = 0.00d;
	}
	
	public Empleado(String nombre, int depto, double salario) {
		this.id_emple = 0;
		this.nombre_emple = nombre;
		this.depto_emple = depto;
		this.salario_emple = salario;
	}

	public int getId_emple() {
		return id_emple;
	}

	public void setId_emple(int id_emple) {
		this.id_emple = id_emple;
	}

	public String getNombre_emple() {
		return nombre_emple;
	}

	public void setNombre_emple(String nombre_emple) {
		this.nombre_emple = nombre_emple;
	}

	public int getDepto_emple() {
		return depto_emple;
	}

	public void setDepto_emple(int depto_emple) {
		this.depto_emple = depto_emple;
	}

	public double getSalario_emple() {
		return salario_emple;
	}

	public void setSalario_emple(double salario_emple) {
		this.salario_emple = salario_emple;
	}
	
	
	
}
