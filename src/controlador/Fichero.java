package controlador;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import Res.Constantes;
import modelo.Empleado;
import vista.FormularioUsuario;

public class Fichero {
	FormularioUsuario form = new FormularioUsuario();
	
	public int asignaCampo() {
		int campo;
		int campo_posicion=0;
		
		do {
			form.elegirCampo();
			campo = form.getNumberInt();
			switch (campo) {
			case 1:
				campo_posicion=4;
				break;
			case 2:
				campo_posicion = 44;
				break;
			case 3:
				campo_posicion = 48;
				break;
			case 0:
				System.out.println(Constantes.CANCELED_MESSAGE);
				break;
			default:
				System.err.println(Constantes.OPTION_ERROR_MESSAGE);
			}
		}while(campo<0 || campo>3);
		 
		return campo_posicion;
	}
	
	public void insertarEmple(Empleado e, String ruta) {
		File fichero = new File(ruta);
		RandomAccessFile raf = null;
		
		try {
			fichero.createNewFile();
			if(fichero.exists()){
				raf = new RandomAccessFile(fichero, "rw");
				
				//Calculo qu� ID tengo que atribuir al empleado que van a insertar:
				if(fichero.length()==0) {
					e.setId_emple(1);
				}else {
					int id_emple=(int)fichero.length()/56;
					id_emple++;
					e.setId_emple(id_emple);
				}
				
				raf.seek(fichero.length());
				
				
				raf.writeInt(e.getId_emple());
				StringBuffer strBuff = new StringBuffer(e.getNombre_emple());
				strBuff.setLength(20);
				raf.writeChars(strBuff.toString());
				raf.writeInt(e.getDepto_emple());
				raf.writeDouble(e.getSalario_emple());
				
				
				raf.close();
				
				System.out.println(Constantes.SUCCESS_PROCESS);
				
			}else {
				System.err.println(Constantes.CREATEFILE_ERROR_MESSAGE);
			}
			
		}catch(FileNotFoundException notfound) {
			System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
		}catch(IOException io) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}catch(Exception ex) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}
	}
	
	public void insertarVariosEmple(ArrayList<Empleado> empleados, String ruta) {
		File fichero = new File(ruta);
		RandomAccessFile raf = null;
		
		try {
			fichero.createNewFile();
			if(fichero.exists()){
				raf = new RandomAccessFile(fichero, "rw");
				
				for (Empleado e : empleados) {
					//Calculo qu� ID tengo que atribuir al empleado que van a insertar:
					if(fichero.length()==0) {
						e.setId_emple(1);
					}else {
						int id_emple=(int)fichero.length()/56;
						id_emple++;
						e.setId_emple(id_emple);
					}
					
					raf.seek(fichero.length());
					
					
					raf.writeInt(e.getId_emple());
					StringBuffer strBuff = new StringBuffer(e.getNombre_emple());
					strBuff.setLength(20);
					raf.writeChars(strBuff.toString());
					raf.writeInt(e.getDepto_emple());
					raf.writeDouble(e.getSalario_emple());
				}
				
				
				
				raf.close();
				
				System.out.println(Constantes.SUCCESS_PROCESS);
				
			}else {
				System.err.println(Constantes.CREATEFILE_ERROR_MESSAGE);
			}
			
		}catch(FileNotFoundException notfound) {
			System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
		}catch(IOException io) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}catch(Exception ex) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}
	}
	
	public ArrayList<Empleado> leeFichero(String ruta) {
		ArrayList<Empleado> empleados = new ArrayList<>();
		File fichero = new File(ruta);
		RandomAccessFile raf = null;
		Empleado e = null;
		
		try {
		
			//Me aseguro de que existe el fichero: 
			if(fichero.exists()) {
				//Me aseguro de que el fichero no est� vac�o: (en if diferente al de fichero.exists() para sacar mensajes diferentes en funci�n de lo que est� pasando)
				if(fichero.length()>0) {
					raf = new RandomAccessFile(fichero, "r");
				
					while(raf.getFilePointer()!=raf.length()) {
						//Me aseguro de que el empleado no est� borrado:
						int cod_emple = raf.readInt();
						if(cod_emple>0) {
							
							//Si existe, leo todo el registro:
							e = new Empleado();
							e.setId_emple(cod_emple);
							
							char[] nombre = new char[20];
							for (int i = 0; i <nombre.length ; i++) {
								nombre[i]=raf.readChar();
							}
							String nombre_emple = new String(nombre);
							
							e.setNombre_emple(nombre_emple);
							e.setDepto_emple(raf.readInt());
							e.setSalario_emple(raf.readDouble());
							
							empleados.add(e);
						//Si no, si es -1 quiere decir que el empleado ha sido borrarlo y por tanto no lo podemos mostrar. 
						//As� que coloco el puntero en el siguiente registro, teniendo en cuenta que ya he movido el puntero
						//4bytes para leer el c�digo. 56-4 =52 bytes para terminar de leer el registro y pasar al siguiente. 
						}else {
							long puntero = raf.getFilePointer()+52;
							raf.seek(puntero);
						}
					}
					
					raf.close();
				}else {
					System.err.println(Constantes.EMPTY_FILE_MESSAGE);
				}
			}else {
				System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
			}
			
		}catch(FileNotFoundException nt) {
			System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
		}catch(IOException io) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}catch (Exception ex) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}
		
		return empleados;
	}
	
	public boolean leeEmpleado(long posicion, String ruta) {
		boolean existe = false;
		File fichero = new File(ruta);
		RandomAccessFile raf = null;
		Empleado e = null;
		
		try {
			if(fichero.exists()) {
				raf = new RandomAccessFile(fichero, "r");
				
				posicion--;
				posicion*=56;
				
				//Me aseguro de que el empleado existe en el fichero:
				if(posicion>=0 && posicion<raf.length()) {
					
					raf.seek(posicion);
					
					//Me aseguro de que a�n existiendo el registro, no se haya borrado previamente. 
					int cod_emple = raf.readInt();
					if(cod_emple>0) {
						
						e = new Empleado();
						e.setId_emple(cod_emple);
						
						char[] nombre = new char[20];
						for (int i = 0; i <nombre.length ; i++) {
							nombre[i]=raf.readChar();
						}
						String nombre_emple = new String(nombre);
						
						e.setNombre_emple(nombre_emple);
						e.setDepto_emple(raf.readInt());
						e.setSalario_emple(raf.readDouble());
						
						//Muestro el empleado consultado:
						form.muestraEmpleado(e);
						
						existe = true;
					}else {
						System.err.println(Constantes.DELETED_EMPLE_MESSAGE);
					}
				}else {
					System.err.println(Constantes.NOTEXISTING_EMPLE_MESSAGE);
				}
				
				
				raf.close();
			}else {
				System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
			}
				
		}catch(FileNotFoundException nt) {
			System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
		}catch(IOException io) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}catch (Exception ex) {
			System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
		}
		
		return existe;
		
	}
	
	
	
	public void borraEmpleado(long posicion, String ruta) {
		//Primero mostramos el empleado que va a proceder a borrar:
		boolean existe = leeEmpleado(posicion, ruta);
		
		//Como resultado de la l�nea anterior, el empleado puede existir o no. 
		//Solo lo borraremos si existe:
		if(existe) {
			//Nos aseguramos de que este es el empleado que desea borrar:
			if(form.ConfirmarOperacion()) {
				RandomAccessFile raf = null;
				File fichero = new File(ruta);
				
				try {
					if(fichero.exists()) {
						raf = new RandomAccessFile(fichero, "rw");
						
						//Coloco el puntero:
						posicion--;
						posicion*=56;
						raf.seek(posicion);
						
						raf.writeInt(-1);
						
						raf.close();
						
						System.out.println(Constantes.SUCCESS_PROCESS);
					}else {
						System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
					}
					
				}catch(IOException e) {
					System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
				}catch(Exception e) {
					System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
				}
			}else {
				System.out.println(Constantes.CANCELED_MESSAGE);
			}
		}
		
	}
	
	public void modificaEmpleado(long posicion, String ruta) {
		//Primero mostramos el empleado que va a proceder a borrar:
		boolean existe = leeEmpleado(posicion, ruta);

		//Como resultado de la l�nea anterior, el empleado puede existir o no. 
		//Solo lo modificaremos si existe:
		if(existe) {
			//Le pediremos la informaci�n que quiere cambiar y le daremos la opci�n de cancelar:
			int campo = asignaCampo();
			//Ahora la debe introducir, si ha elegido continuar, pero los tipos de datos son diferentes en cada caso, por tanto:
			String nombre_nuevo = null;
			int dpto_nuevo = 0;
			double salario_nuevo = 0.00d;
			
			if(campo!=0) {
				switch(campo) {
					case 4:
						System.out.println(Constantes.EMPLE_NAME_REQUEST);
						nombre_nuevo = form.getText();
						break;
					case 44:
						System.out.println(Constantes.EMPLE_DPTO_REQUEST);
						dpto_nuevo= form.getNumberInt();
						break;
					case 48:
						System.out.println(Constantes.EMPLE_SALARIO_REQUEST);
						salario_nuevo = form.getNumberDouble();
						break;
				}
				
				//Sacamos mensaje de confirmaci�n antes de tocar nada:
				if(form.ConfirmarOperacion()) {
					//Modificamos el archivo:
					RandomAccessFile raf = null;
					File fichero = new File(ruta);
					
					try {
						if(fichero.exists()) {
							raf = new RandomAccessFile(fichero, "rw");
							
							//Coloco el puntero (posicion del registro):
							posicion--;
							posicion*=56;
							//(+posici�n del campo de dicho registro):
							posicion= (long)campo + posicion;
							raf.seek(posicion);
							
							//Modifico registros:
							switch(campo) {
								case 4:
									StringBuffer strBuff = new StringBuffer(nombre_nuevo);
									strBuff.setLength(20);
									raf.writeChars(strBuff.toString());
									System.out.println(Constantes.SUCCESS_PROCESS);
									break;
								case 44:
									raf.writeInt(dpto_nuevo);
									System.out.println(Constantes.SUCCESS_PROCESS);
									break;
								case 48:
									raf.writeDouble(salario_nuevo);
									System.out.println(Constantes.SUCCESS_PROCESS);
									break;
							}
							
							raf.close();
						}else {
							System.err.println(Constantes.NOTFOUND_ERROR_MESSAGE);
						}
					}catch(IOException e) {
						System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
					}catch (Exception e) {
						System.err.println(Constantes.EXCEPTION_ERROR_MESSAGE);
					}
					
				}else {
					System.out.println(Constantes.CANCELED_MESSAGE);
				}
				
			}
		}
		
	}
	
	public double buscaMayorSueldo(ArrayList<Empleado> empleados) {
		
		
		double sueldo=0.00d;
		for (Empleado empleado : empleados) {
			if(sueldo<empleado.getSalario_emple()) {
				sueldo = empleado.getSalario_emple();
			}
		}
		
		
		return sueldo;
	}
	
	
	public void borradoFisico(ArrayList<Empleado> e) {
		 insertarVariosEmple(e, Constantes.RUTA_FILE_COPIA);
		 
		 File fichero = new File(Constantes.RUTA_FILE);
		 File fichero_copia = new File(Constantes.RUTA_FILE_COPIA);
		 
		 if (!fichero.delete() || !fichero_copia.renameTo(fichero)){
			System.err.println(Constantes.RENAMING_DELETING_ERROR);
		 }
		 
	}
	
	public void getDptoMasEmple(ArrayList<Empleado> empleados) {
		HashMap<Integer, Integer> emple_por_dpto = new HashMap<>();
		
		for (Empleado empleado : empleados) {
			if(emple_por_dpto.containsKey(empleado.getDepto_emple())) {
				int num_emple = emple_por_dpto.get(empleado.getDepto_emple());
				num_emple++;
				emple_por_dpto.put(empleado.getDepto_emple(), num_emple);
			}else {
				emple_por_dpto.put(empleado.getDepto_emple(), 1);
			}
		}
		
		Integer max = Collections.max(emple_por_dpto.entrySet(), Comparator.comparingInt(HashMap.Entry::getValue)).getKey();
		
		//Muestra el departamento con m�s empleados y el n�mero de empleados:
		form.muestraDptoMasEmple(emple_por_dpto, max);
		
	}
	
	public void getDptoMasSalario(ArrayList<Empleado> empleados) {
		HashMap<Integer, Double> gasto_dpto = new HashMap<>();
		
		for (Empleado empleado : empleados) {
			if(gasto_dpto.containsKey(empleado.getDepto_emple())) {
				double salario_emple = gasto_dpto.get(empleado.getDepto_emple());
				salario_emple+=empleado.getSalario_emple();
				gasto_dpto.put(empleado.getDepto_emple(), salario_emple);
			}else {
				gasto_dpto.put(empleado.getDepto_emple(), empleado.getSalario_emple());
			}
		}
		
		Integer max = Collections.max(gasto_dpto.entrySet(), Comparator.comparingDouble(HashMap.Entry::getValue)).getKey();
		
		//Muestra el departamento con m�s gasto y la suma del salario:
		form.muestraDptoMasGasto(gasto_dpto, max);
				
		
	}
	
	
	
	
}
