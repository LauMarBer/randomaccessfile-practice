package controlador;

import java.util.ArrayList;

import Res.Constantes;
import modelo.Empleado;
import vista.FormularioUsuario;

public class Main {

	public static void main(String[] args) {
		ArrayList<Empleado> empleados = new ArrayList<>();;
		FormularioUsuario form = new FormularioUsuario();
		Fichero fich = new Fichero();
		
		
		int opcion;
		do {
			form.muestraMenu();
			opcion = form.getNumberInt();
			switch(opcion) {
				case 1:
					empleados = fich.leeFichero(Constantes.RUTA_FILE);
					form.tablaCase01(empleados);
					break;
				case 2:
					System.out.println(Constantes.EMPLE_COD_REQUEST);
					fich.leeEmpleado(form.getNumberLong(), Constantes.RUTA_FILE);
					break;
				case 3: 
					fich.insertarEmple(form.introduceEmpleado(), Constantes.RUTA_FILE);
					break;
				case 4:
					System.out.println(Constantes.EMPLE_COD_REQUEST);
					fich.borraEmpleado(form.getNumberLong(), Constantes.RUTA_FILE);
					break;
				case 5:
					System.out.println(Constantes.EMPLE_COD_REQUEST);
					fich.modificaEmpleado(form.getNumberLong(), Constantes.RUTA_FILE);
					break;
				case 6:
					empleados = fich.leeFichero(Constantes.RUTA_FILE);
					form.muestraEmpleMayorSueldo(empleados, fich.buscaMayorSueldo(empleados));
					break;
				case 7:
					fich.borradoFisico(fich.leeFichero(Constantes.RUTA_FILE));
					break;
				case 8:
					fich.getDptoMasEmple(fich.leeFichero(Constantes.RUTA_FILE));
					break;
				case 9:
					fich.getDptoMasSalario(fich.leeFichero(Constantes.RUTA_FILE));
					break;
				case 0: 
					System.out.println(Constantes.BYE_MESSAGE);
					break;
				default:
					System.err.println(Constantes.OPTION_ERROR_MESSAGE);
			}
			
		}while(opcion!=0);

	}

}
