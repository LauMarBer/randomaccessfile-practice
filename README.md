Objetivo: practicar ficheros de Aceso Directo (AD). 

Gestionar los datos de los empleados de una empresa, utilizando ficheros de AD. Para cada empleado se almacena: código (numero entero secuencial autoincrementable - 4bytes), nombre (cadena de 20 chars), depto (numero entero - 4bytes) y salario (double - 8bytes).

La aplicación consiste en un menú con las siguientes opciones:
1. Mostrar todos los empleados
2. Mostrar un empleado. Se pedirá el código de un empleado y se mostrarán todos sus datos
3. Insertar empleado. Esta opción insertará datos de un empleado al final del archivo. El código se calcula según el número de registro que corresponda. Al primer empleado que se inserte se le pondrá código 1.
4. Borrar empleado. Se pedirá el código de un empleado y se pondrá -1 en su código. Se deben mostrar mensajes de error si un empleado pedido no existe, si el archivo está vacío o no se encuentra, etc.
5. Modificar un empleado, pediremos el número de un empleado, le mostraremos todos los datos y luego le pedimos los nuevos de la forma en que queramos.
6. Mostrar los empleados que más ganan. 
7. Borrado físico. En este caso se trata de eliminar a todos los empleados que tienen el -1 y reorganizar a todos los demás actualizando su código según el orden en que aparecen en el registro. 
8. Mostrar el departamento que más empleados tiene. 
9. Mostrar el departamento que más gasta en salarios. 


-----------------------------------------------------------------------------------------------------------------------------------------------

Objective: to practise Direct Access (DA) files. 

To manage the data of the employees of a company, using AD files. For each employee the following are stored: code (auto-incrementable sequential integer number - 4bytes), name (string of 20 chars), department (integer - 4bytes) and salary (double - 8bytes).

The application consists of a menu with the following options:
1. Show all employees.
2. Show one employee. The code of an employee will be requested and all his data will be shown.
3. Insert employee. This option will insert data of an employee at the end of the file.The code is calculated according to the corresponding record number. The first employee to be inserted will be given code 1.
4. Delete employee. The code of an employee will be requested and the employee's code will be set to -1.Error messages should be displayed if a requested employee does not exist, if the file is empty or not found, etc.
5. Modify an employee, we will ask for an employee's number, show all the data and then ask for the new data in any way we want.
6. Show the employees who earn the most. 
7. Physical deletion. In this case it is a question of eliminating all the employees who have the -1 and reorganising all the others by updating their code according to the order in which they appear in the register. 
8. Show the department with the most employees. 
9. Show the department that spends the most on salaries. 
